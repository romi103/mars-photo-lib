import axios from "axios";

const APIkey = process.env.VUE_APP_API;
const urlBase = "https://mars-photos.herokuapp.com/api/v1/"

export default {
    getManifest(rover) {
        return axios.get(`${urlBase}/manifests/${rover}?api_key=${APIkey}`)
    },

    getPhotos(rover, sol, camera) {
        return axios.get(`${urlBase}/rovers/${rover}/photos?sol=${sol}&camera=${camera}&api_key=${APIkey}`)
    }
}