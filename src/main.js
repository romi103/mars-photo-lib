import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import moment from "vue-moment";

import vSelect from 'vue-select';
import TextInput from './components/TextInput.vue';
import Capitalize from './filters/capitalize.js';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret, faSpinner)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.component('v-select', vSelect);
Vue.component('TextInput', TextInput);
Vue.use(moment);

Vue.filter('capitalize', Capitalize);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
